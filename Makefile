all:
	GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o hello main.go
	sudo docker build -t hello .
	sudo docker run hello
